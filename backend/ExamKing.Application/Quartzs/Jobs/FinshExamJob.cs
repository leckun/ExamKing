using System;
using System.Threading.Tasks;
using ExamKing.Application.Services;
using Furion;
using Microsoft.Extensions.Logging;
using Quartz;

namespace ExamKing.Application.Quartzs
{
    /// <summary>
    /// 考试作业
    /// </summary>
    [DisallowConcurrentExecution]
    public class FinshExamJob : IJob
    {
        /// <summary>
        /// 依赖服务
        /// </summary>
        private readonly ILogger<FinshExamJob> _logger;
        
        ///  <summary>
        /// 依赖注入
        ///  </summary>
        ///  <param name="logger"></param>
        public FinshExamJob(
            ILogger<FinshExamJob> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 执行任务
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Execute(IJobExecutionContext context)
        {
            JobDataMap dataMap = context.JobDetail.JobDataMap;
            int jobId = dataMap.GetIntValue("jobId");
            
            _logger.LogInformation($"Finsh jobId=>{jobId} is running");
            
            var examJobService = App.GetService<IExamJobService>();
            // 执行考试任务
            await examJobService.ExecuteFinshExamJob(jobId);

        }
    }
}