using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ExamKing.Core.Entites;

namespace ExamKing.Application.Services
{
    
    /// <summary>
    /// 考试任务调度服务
    /// </summary>
    public interface IExamJobService
    {
        /// <summary>
        /// 还原未完成到考试任务
        /// </summary>
        /// <returns></returns>
        public Task<List<TbExamjobs>> RestartExamJobAll();

        /// <summary>
        /// 新增考试任务
        /// </summary>
        /// <param name="examjob"></param>
        /// <returns></returns>
        public Task AddExamJob(TbExamjobs examjob);
        
        /// <summary>
        /// 更新考试任务
        /// </summary>
        /// <param name="examjob"></param>
        /// <returns></returns>
        public Task<bool> UpdateExamJob(TbExamjobs examjob);
        
        /// <summary>
        /// 执行考试任务
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<bool> doExamJob(TbExamjobs examjobs);

        /// <summary>
        /// 执行开始考试任务
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<bool> ExecuteStartExamJob(int id);
        
        /// <summary>
        /// 执行结束考试任务
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<bool> ExecuteFinshExamJob(int id);

        public Task<bool> DelStartExamJob(string examId);
        
        public Task<bool> DelFinshExamJob(string examId);

    }
}