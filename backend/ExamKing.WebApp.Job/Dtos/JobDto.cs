namespace ExamKing.WebApp.Job.Dtos
{
    public class JobDto
    {
        
        public string jobGroup { get; set; }
        public string jobName { get; set; }
        public string triggerTime { get; set; }
    }
}

