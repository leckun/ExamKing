import request from "@/utils/request";

export const list = (params) => {
  return request({
    url: "/score/exam-score-list",
    method: "get",
    params,
  });
};

export default {};
